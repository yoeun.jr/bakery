package com.bakery;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {
		@RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
		public String index(Model model) {
			model.addAttribute("pageName", "index");
			return "default";
		}
		@RequestMapping(value = {"/about"}, method = RequestMethod.GET)
		public String about(Model model) {
			model.addAttribute("pageName", "about");
			return "default";
		}
		@RequestMapping(value = {"/contact"}, method = RequestMethod.GET)
		public String contact(Model model) {
			model.addAttribute("pageName", "contact");
			return "default";
		}
		@RequestMapping(value = {"/gallery"}, method = RequestMethod.GET)
		public String gallery(Model model) {
			model.addAttribute("pageName", "gallery");
			return "default";
		}
		@RequestMapping(value = {"/icons"}, method = RequestMethod.GET)
		public String icons(Model model) {
			model.addAttribute("pageName", "icons");
			return "default";
		}
		@RequestMapping(value = {"/services"}, method = RequestMethod.GET)
		public String services(Model model) {
			model.addAttribute("pageName", "services");
			return "default";
		}
		@RequestMapping(value = {"single"}, method = RequestMethod.GET)
		public String single(Model model) {
			model.addAttribute("pageName", "single");
			return "default";
		}
		@RequestMapping(value = {"typography"}, method = RequestMethod.GET)
		public String typography(Model model) {
			model.addAttribute("pageName", "typography");
			return "default";
		}
}
